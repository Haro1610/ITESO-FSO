#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <pthread.h>

# define NTHREADS 8
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

#define SIZE 4000 // define el tamaño de las matriz la cantidad de elementos que tentra en filas y columnas

#define INICIAL 900000000 // 900_000_000 novecientos millones
#define FINAL 1000000000 // 1_000_000_000 mil millones

int mat[SIZE][SIZE];// el valor  de size se asgina tanto a filas como a columnas se define el tamaño de la matriz

void initmat(int mat[][SIZE]);
void printnonzeroes(int mat[SIZE][SIZE]);
int isprime(int n);

struct ThreadArgs {
    int startRow;
    int endRow;
};
//void tarea(void *args);
void *tarea(void *arg)
{
    //int i, j;
    struct ThreadArgs *args = (struct ThreadArgs *)arg;

    for (int i = args->startRow; i <= args->endRow; i++) {
        for (int j = 0; j < SIZE; j++) {
            if (!isprime(mat[i][j])) {
                mat[i][j] = 0;
            }
        }
    }
	return NULL;
}


int main()
{
	long long start_ts;
	long long stop_ts;
	long long elapsed_time;
	long lElapsedTime;
	struct timeval ts;
	//int i,j;

	// Inicializa la matriz con números al azar
	initmat(mat); // se llama a la función que llenarla de numeros la matriz vacía

	
	// se usa para calcular el timpo que tomo la ejecución del programa
	gettimeofday(&ts, NULL);
	start_ts = ts.tv_sec; // Tiempo inicial

	// Eliminar de la matriz todos los números que no son primos
	// Esta es la parte que hay que paralelizar
	
	// se recorre todoa la matriz y se evalua si es número primo si no es un número primos se sobreescribe con 0
	// esa posición de la matriz si es primo se deja el número tal caul

	pthread_t tid[NTHREADS];
	struct ThreadArgs threadArgs[NTHREADS];

	int rowsPerThread = SIZE / NTHREADS;
    int remainingRows = SIZE % NTHREADS;

    for (int i = 0; i < NTHREADS; i++) {
        threadArgs[i].startRow = i * rowsPerThread;
        threadArgs[i].endRow = (i + 1) * rowsPerThread - 1;
        if (i == NTHREADS - 1) {
            threadArgs[i].endRow += remainingRows;
        }
    }
    

    for(int i=0;i<NTHREADS;i++)
        pthread_create(&tid[i],NULL,tarea,&threadArgs[i]);

    for(int i=0;i<NTHREADS;i++)
        pthread_join(tid[i],NULL);

	
	
	// Hasta aquí termina lo que se tiene que hacer en paralelo
	gettimeofday(&ts, NULL);
	stop_ts = ts.tv_sec; // Tiempo final
	elapsed_time = stop_ts - start_ts;


	printnonzeroes(mat);
	printf("------------------------------\n");
	printf("TIEMPO TOTAL, %lld segundos\n",elapsed_time);
}

/*
la función initmat se encarga 
*/
void initmat(int mat[][SIZE])
{
	int i,j;
	int counter =0;
	
	srand(getpid()); // genra el valor aleatoria a partir del process id con una semilla que cambia constantemente
	

	for(i=0;i<SIZE;i++)
		for(j=0;j<SIZE;j++)
			mat[i][j]= INICIAL + rand() % (FINAL-INICIAL);// para cada posición de la matriz se insertara el valor 
			//mat[i][j] = counter++;
			/*
				aleatorio genrado a patrir de srand + El valor de la constante INICIAL hacinedo un and con la resta FINAL- INICIAL
				esto equivales a sumar el número aleatorio mas mill 1000 y hacer un and con 100 millones y resultado de esa operación
				es lo que se guradara en cada posicion de la matriz
			*/ 
}

// imprime todos los valores de la matriz que fueron catalagados como ceros
void printnonzeroes(int mat[SIZE][SIZE])
{
	int i,j;
	
	for(i=0;i<SIZE;i++)
		for(j=0;j<SIZE;j++)
			if(mat[i][j]!=0)
				printf("%d\n",mat[i][j]);
}

			   
/*
La función evalua el valor almacenado en cad posición 
de la matriz y calcula si es un numero primo o no
si es un número primo retorna 1 y si no  0
*/   
int isprime(int n)
{
	int d=3;
	int prime=n==2;
	int limit=sqrt(n);
	
	if(n>2 && n%2!=0)
	{
		while(d<=limit && n%d)
			d+=2;
		prime=d>limit;
	}
	return(prime);
}