/*
Para compilar incluir la librería m (matemáticas)
Ejemplo:
gcc -o mercator mercator.c -lm
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <semaphore.h>
//#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define NPROCS 4
#define SERIES_MEMBER_COUNT 200000

double x = 1.0;

int *proc_count;
int *msgid;

double *res;


sem_t *mutex;
sem_t *start_barrier;
sem_t *finish_barrier;

struct msgbuf {
    long mtype;
    double mnum;
};


double get_member(int n, double x)
{
    int i;
    double numerator = 1;
    for (i = 0; i < n; i++)
        numerator = numerator * x;
    if (n % 2 == 0)
        return (-numerator / n);
    else
        return numerator / n;
}

void proc(int proc_num)
{


    printf("Proceso %d iniciado\n", proc_num);
    sem_wait(start_barrier);
    printf("Proceso %d desbloqueado\n", proc_num);

    struct msgbuf mensaje;

    int i;
    double num;
    for (i = proc_num; i < SERIES_MEMBER_COUNT; i += NPROCS){

        sem_wait(mutex);
        num = get_member(i + 1, x);
        
        mensaje.mtype = 1; 
        mensaje.mnum = num; 

        msgsnd(*msgid, &mensaje, sizeof(mensaje.mnum), 0);
        sem_post(mutex);


    }

    sem_post(finish_barrier);
    exit(0);
}

void master_proc()
{
    int i;
    sleep(2);

    *res = 0;
    struct msgbuf mensaje;

    printf("Se inicio el proceso padre\n");
    
    int semValue;
    for (i = 0; i < NPROCS; i++){

        sem_getvalue(start_barrier, &semValue);

        printf("Valor actual del semáforo start_barrier: %d\n", semValue);
        sem_post(start_barrier);
    }
        
    

    printf("Proc_count  count   primero     ultimo\n");
    while(*proc_count<SERIES_MEMBER_COUNT){
        
        msgrcv(*msgid, &mensaje, sizeof(mensaje.mnum), 1, 0);
        printf("Mensaje recibido: %f       %d\n", mensaje.mnum,*proc_count);
        *res+=mensaje.mnum;
        *proc_count+=1;

    }

    for (i = 0; i < NPROCS; i++){
        sem_wait(finish_barrier);
        printf("Wait numero %d terminado\n", i);
    }

    exit(0);
}

int main()
{
    //int *threadIdPtr;
    long long start_ts;
    long long stop_ts;
    long long elapsed_time;
    long lElapsedTime;
    struct timeval ts;
    
    int i;    
    int p;
    int shmid;
    void *shmstart;

    shmid = shmget(0x1234, sizeof(double) + 4 * sizeof(int), 0666 | IPC_CREAT);
    shmstart = shmat(shmid, NULL, 0);
    
    proc_count = shmstart;
    msgid = shmstart + sizeof(int);
    res = shmstart + 2 * sizeof(int);

    *proc_count = 0;


    gettimeofday(&ts, NULL);
    start_ts = ts.tv_sec; // Tiempo inicial
    

    int sem_shmid;
    void *sem_shmidStart;

    sem_shmid = shmget(0x1245, 3 * sizeof(sem_t), IPC_CREAT | 0666);
    sem_shmidStart = shmat(sem_shmid, 0, 0);

    start_barrier = sem_shmidStart;
    mutex = sem_shmidStart + sizeof(sem_t);
    finish_barrier = sem_shmidStart + 2*sizeof(sem_t);

    sem_init(mutex,1,1);
    sem_init(start_barrier,1,0);
    sem_init(finish_barrier,1,0);

    key_t key = ftok("/tmp/clave_file", 'A');
    *msgid = msgget(key, IPC_CREAT | 0666);

    for (i = 0; i < NPROCS; i++)
    {
        p = fork();
        if (p == 0)
            proc(i);
    }

    p = fork();
    if (p == 0)
        master_proc();

//    printf("El recuento de ln(1 + x) miembros de la serie de Mercator es %d\n", SERIES_MEMBER_COUNT);
//    printf("El valor del argumento x es %f\n", (double)x);

    for (int i = 0; i < NPROCS + 1; i++)
        wait(NULL);

    msgctl(*msgid, IPC_RMID, NULL);

    gettimeofday(&ts, NULL);
    stop_ts = ts.tv_sec; // Tiempo final
    elapsed_time = stop_ts - start_ts;
    printf("Tiempo = %lld segundos\n", elapsed_time);
    printf("El resultado es %10.8f\n", *res);
    //printf("Llamando a la función ln(1 + %f) = %10.8f\n", x, log(1 + x));
    shmdt(shmstart);
    shmctl(shmid, IPC_RMID, NULL);
}