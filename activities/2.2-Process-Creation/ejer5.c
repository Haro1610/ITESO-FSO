#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h> // libería del wait


int main() {

    

    //return 0;
    char program_path[1000];
    const char exit_loop[] = "exit";

    while (1)
    {
        printf("Ingresa la ruta del archivo que quieres ejecutar: ");
        fgets(program_path, sizeof(program_path), stdin);

        size_t length = strlen(program_path);
        if (length > 0 && program_path[length - 1] == '\n') {
            program_path[length - 1] = '\0';
        }

        if (strcmp(program_path, exit_loop) == 0) {  
            printf("¡Has ingresado exit, Fin del programa\n");
            break; // Sale del ciclo
        }

        //execl("/home/fso/Documentos/Actividad3", "sudo ./ejer2", NULL);

        int p = fork();
        

        if (p == 0) {
            execl("/usr/bin/sudo", "/usr/bin/sudo", program_path, NULL);
        } 
        else {
            wait(NULL);
            printf("Ha teminado el programa que indicaste, ingresa la ruta de otro progama si lo deseas o exit para salir \n");
        }
        

    }
}
