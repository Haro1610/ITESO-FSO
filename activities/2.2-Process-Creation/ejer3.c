#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main() 

{   int num_procesos;
    printf("Ingresa el numero de hijos a crear -> ");
    scanf("%d", &num_procesos);

    int counter = 1;
    printf("Proceso padre -> %d \n",getpid());

    for (int i=0;i<num_procesos;i++) { 

        int c = fork(); 

        
        
        if(c == 0) 
        { 
            printf("Proceso hijo %d pid -> %d\n",counter,getpid()); 
            
            exit(0); 
            // se usa exit para terminar cada proceso hijo justo despues de 
            // creación para así evirtar que se cren una cantidad enorme de proceso hijos
            // y así poder controlar la cantidad exacta de procesos que queremos crear
        }

        
        counter ++;
    }

    for(int i=0;i<num_procesos;i++) // necesitamos que el proceso padre
    // espera que sus porceos hijos terminen por lo cual de dampos la misma duración 
    // y un wait
    
    wait(NULL); 
    printf("Fin, proceso padre ha terminado-> %d \n",getpid());
    

}