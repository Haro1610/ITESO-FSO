#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // libería de de fork()
#include <sys/wait.h> // libería del wait

int main() {
    //int p ;
    int p = fork(); // se generan un pid para el proceso hijo a partir del pid del proceso padre el cual
    // esta implicito desde el comienzo de la ejeucución del codigo
    //printf("Este es el valor de p %d\n",p);

    if (p == 0) {
        
        for (int i = 0; i < 10; i++) {
            printf("Soy el hijo %d\n",getpid());
            fflush(stdout);
            sleep(1); 
        }
        // los saltos en procesos padre-hijo se van dando de uno en uno
        exit(0); 
    } else {

        for (int i = 0; i < 10; i++) {
            printf("Soy el padre %d\n",getpid());
            fflush(stdout);
            sleep(1); 
        }
        
        
        wait(NULL);
        
        printf("Mi proceso hijo ya ha terminado\n");
    }

    return 0;
}
// dependiendo de la compilación es como se ejecutaran los proceos no se puede predecir
// el orden en el que terminaran ya que eso lo decide el planificador y como este asigna los recuroso
// no esta a nuestro alance para este código