#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{

    int nivel;
    
    printf("Ingresa un número: ");
    scanf("%d", &nivel);  // Lee un número entero y lo almacena en la variable 'nivel'
    for (int i = 0; i <= nivel; i++){
        printf("Este es el nivel %d\n",i);
        fflush(stdout);
        //sleep(1);
        fork();
    }
    
}