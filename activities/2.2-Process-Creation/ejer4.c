#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main() {

    char comando_system[100]; // Declara un arreglo para almacenar  comando_system que será interpretada como un comando del ssitema
    const char exit_loop[] = "exit"; // contra esta comando_system vamos a comparar si se termina el cliclo

    while (1)
    {
        printf("Ingresa un comando: ");
        fgets(comando_system, sizeof(comando_system), stdin); // se le la comando_system del input

        // se formante a fgest para quitar el salto de línea
        size_t length = strlen(comando_system);
        if (length > 0 && comando_system[length - 1] == '\n') {
            comando_system[length - 1] = '\0';
        }

        if (strcmp(comando_system, exit_loop) == 0) { // si el comando de entrada es igual 
        // a la cadena exit se termina el ciclo
            printf("¡Has ingresado exit, Fin del programa\n");
            break; // Sale del ciclo
        }

        int resultado = system(comando_system); // Ejecuta el comando

        if (resultado == 0) {
            printf("El comando se ejecutó correctamente.\n");
        } 
        else {
            printf("Hubo un error al ejecutar el comando.\n");
        }

    }
    
    return 0;
}
