#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
    fork();
    printf("Hola soy el padre, mi pid es %d\n",getpid());
    
    printf("Adios, mi pid es %d\n",getpid());
}