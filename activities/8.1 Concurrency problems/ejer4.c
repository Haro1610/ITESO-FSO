#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <semaphore.h>
#include <fcntl.h>

sem_t *s, *t; // Definiciones globales para los semáforos

void imprime(char *s)
{
    usleep(rand()%100000);
    printf("%s\n",s);
    usleep(rand()%100000);
}

void P()
{
    sem_wait(s);

    imprime("A");
    imprime("B");
    

    sem_post(t);
    imprime("C");

    exit(0);
}

void Q()
{
    sem_wait(t);

    imprime("D");
    imprime("E");

    sem_post(s);

    exit(0);
}

int main() 
{
    int p;

    srand(getpid());

    s = sem_open("s", O_CREAT, 0644, 1);
    t = sem_open("t", O_CREAT, 0644, 0);

    p = fork();
    if(p == 0)
        P();

    p = fork();
    if(p == 0)
        Q();

    wait(NULL);
    wait(NULL);

    sem_close(s);
    sem_close(t);
    sem_unlink("s");
    sem_unlink("t");

    return 0;
}
