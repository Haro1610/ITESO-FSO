#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <pthread.h>

#define N_PROCESSES 8
#define SECCIONES 50000000 // define el numero de secciones en las que se va a dividir el cuarto de circulo
#define ALTURA(a) (sqrt(1 - (a) * (a)))
#define AREA_TRAPECIO(a, b, c) ((c) * ((b) + ((a) - (b)) / 2))

double *g;

struct ProcessArgs
{
    double inicio;
    double final;
};

double AREA_SECCION(void *arg)
{
    struct ProcessArgs *args = (struct ProcessArgs *)arg;

    double inicio = args->inicio;
    double final = args->final;

    //printf("%lf %lfd\n",inicio,final);

    double inc;
    double base = (1.0 / (SECCIONES))/N_PROCESSES;
    double area = 0.0;

    for (double i = inicio; i < final; i++)
    {
        double x1 = i * base;
        double x2 = (i+1) * base; 
        

        double y1 = sqrt(1 - (x1) * (x1));
        double y2 = sqrt(1 - (x2) * (x2)); 

        inc = base * (y1+y2)/2;
        area+=inc;

        
    }
    //printf("%lf d\n",area);
    return area;
}

int main()
{
    long long start_ts;
    long long stop_ts;
    long long elapsed_time;
    long lElapsedTime;
    struct timeval ts;

    // se usa para calcular el timpo que tomo la ejecución del programa
    gettimeofday(&ts, NULL);
    start_ts = ts.tv_sec; // Tiempo inicial

 //   pthread_t tid[N_PROCESSES];
    struct ProcessArgs processArgs[N_PROCESSES];

    for (int i = 0; i < N_PROCESSES; i++)
    {
        processArgs[i].inicio = i * SECCIONES;
        processArgs[i].final = (i+1) * SECCIONES;
    }


    int shmid = shmget(0x1234,sizeof(double),0666|IPC_CREAT);
    g = shmat(shmid,NULL,0);
    *g = 0.0;

    int p = fork();

    if(p == 0){
        for (int i = 0 ; i < N_PROCESSES ; i++){
            *g += AREA_SECCION(&processArgs[i]);
            printf("%lf \n",*g);
        } 
    }else{
        wait(NULL);
        gettimeofday(&ts, NULL);
        stop_ts = ts.tv_sec; // Tiempo final
        elapsed_time = stop_ts - start_ts;

        printf("------------------------------\n");
        printf("PI es igual a %f \n", *g*4);
        printf("TIEMPO TOTAL, %lld segundos\n", elapsed_time);
    }

    // Hasta aquí termina lo que se tiene que hacer en paralelo
/*
    gettimeofday(&ts, NULL);
    stop_ts = ts.tv_sec; // Tiempo final
    elapsed_time = stop_ts - start_ts;

    printf("------------------------------\n");
    printf("PI es igual a %f \n", *g*4);
    printf("TIEMPO TOTAL, %lld segundos\n", elapsed_time);
*/
}