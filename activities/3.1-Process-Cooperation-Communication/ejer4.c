# include <stdio.h>
# include <stdlib.h>
# include <sys/wait.h>
# include <unistd.h>
#include <signal.h>

// crear un un hijo con un exit dentro para que puede detener la ejecución de su padre
/* 
se debe usar kill para matar un proceso y su process id para mandar la seña a ese proceso en especifico
*/

#include <stdio.h>
#include <unistd.h>

int main() {
    int child1, child2;

    // Crear el primer proceso hijo
    child1 = fork();



    if (child1 == 0) {
        // Este es el primer proceso hijo
        printf("Soy el primer proceso hijo  %d\n", getpid());
        sleep(20);

        int counter = 1;
        for (int i=0;i<3;i++) { 
            int c = fork();         
            if(c == 0) { 
                printf("Proceso nieto %d pid -> %d\n",counter,getpid()); 
                sleep(20);
            
                exit(0); 
            }
            counter ++;
        }
        wait(NULL);
    
    } 
    else {
        // Este es el proceso padre
        // Crear el segundo proceso hijo
        child2 = fork();

        if (child2 == 0) {
            // Este es el segundo proceso hijo
            printf("Soy el segundo proceso hijo  %d\n", getpid());
            sleep(20);
            int counter = 4;
            for (int i=0;i<3;i++) { 
                int c = fork();         
                if(c == 0) { 
                    printf("Proceso nieto %d pid -> %d\n",counter,getpid()); 
                    sleep(20);
            
                    exit(0); 
                }
                counter ++;
            }
            wait(NULL);
        } 
        else {
            // Este es el proceso padre
            printf("Soy el proceso padre %d\n", getpid());
            sleep(5);
            //kill(child1, SIGTERM);
            //kill(child2, SIGTERM);

            // Esperar a que ambos procesos hijos terminen
            wait(NULL);
            wait(NULL);
            printf("Fin del proceso padre \n");

        }
    }

    return 0;
}
