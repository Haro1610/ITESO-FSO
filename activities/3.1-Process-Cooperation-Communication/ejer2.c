#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


int main() {
    
    int p = fork();

    if (p == 0) {
        // Este es el proceso hijo
        printf("Ejecutando el proceso hijo comienza... %d\n",getpid());
        sleep(1); 
        printf("Proceso hijo finalizado.\n");
    } 
    else if (p != 0)
    {
        printf("Proceso padre esperando 20 segundos... %d \n", getpid());
        //system("ps");
        sleep(20); // Espera 20 segundos
        //wait(NULL); // Espera a que termine el proceso hijo
        printf("Proceso padre finalizado.\n");

   
    }


    return 0;
}
