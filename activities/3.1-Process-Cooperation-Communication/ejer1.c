# include <stdio.h>
# include <stdlib.h>
# include <sys/wait.h>
# include <unistd.h>
#include <signal.h>

// crear un un hijo con un exit dentro para que puede detener la ejecución de su padre
/* 
se debe usar kill para matar un proceso y su process id para mandar la seña a ese proceso en especifico
*/


int main(){

    printf("Este es el vaor del padre %d \n", getpid());

    int p = fork();

    if(p == 0){ // aquí debe morir el padre este es el hijo
        kill(p, SIGTERM);
        printf("Soy el hijo %d, pero mate a mi padre  %d \n",p,getpid());
        
        
    }else{
        printf("Soy el padre muriendo %d \n", getpid());
        wait(NULL);// si no se muestra esta línea siginifca que el padre murito antes de terminar de ejercutarse
        
    }
    return 0;

}